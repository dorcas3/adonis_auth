# Interns AUTH Api

An adonis application that exposes CRUD functionalities for interns

## link to Endpoints

[InternsEndpoints](https://documenter.getpostman.com/view/10658641/TzRRBnt5)

## user requirements

 - Seed the users table with at least 500,000 users and 1,500,000 stack relationships across multiple years of internships,i.e from 2000 all the way to 2020.

 - Fetch interns that were in a particular cohort through an endpoint.

 - A user should not be able to see the stacks of another intern unless they are an administrator.

 
## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
