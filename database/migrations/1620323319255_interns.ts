import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Interns extends BaseSchema {
  protected tableName = 'interns'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('username').notNullable()
      table.string('email').notNullable().unique()
      table.string('month').notNullable()
      table.integer('year',4).notNullable()
      table.timestamps(true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
