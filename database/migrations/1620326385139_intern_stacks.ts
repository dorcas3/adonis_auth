import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class StackUsers extends BaseSchema {
  protected tableName = 'intern_stack'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('intern_id')
        .unsigned()
        .references('id')
        .inTable('interns')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table.integer('stack_id')
        .unsigned()
        .references('id')
        .inTable('stacks')
        .onUpdate('CASCADE')
        .onDelete('SET NULL')
      table.timestamps(true)
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
