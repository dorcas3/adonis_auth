import { DateTime } from 'luxon'
import { BaseModel, column,  manyToMany,ManyToMany, } from '@ioc:Adonis/Lucid/Orm'
import Stack from 'App/Models/Stack'
import InternFilter from 'App/Models/Filters/InternFilter'
import { filterable } from '@ioc:Adonis/Addons/LucidFilter'


@filterable (InternFilter)
export default class Intern extends BaseModel {

  @column({ isPrimary: true })
  public id: number

  @column()
  public username: string

  @column()
  public email: string

  @column()
  public month: string

  @column()
  public year: number

  @manyToMany(() => Stack, {
    localKey: 'id',
    pivotForeignKey: 'intern_id',
    relatedKey: 'id',
    pivotRelatedForeignKey: 'stack_id',
  })
  public stacks: ManyToMany<typeof Stack>

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
