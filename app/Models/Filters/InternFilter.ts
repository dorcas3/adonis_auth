import { BaseModelFilter } from '@ioc:Adonis/Addons/LucidFilter'
import { ModelQueryBuilderContract } from '@ioc:Adonis/Lucid/Orm'
import Intern from 'App/Models/Intern'

export default class InternFilter extends BaseModelFilter {
  public $query: ModelQueryBuilderContract<typeof Intern, Intern>

  // public method(value: any): void {
  //   this.$query.where('name', value)
  // }
  


  month(month: string) {
    this.$query.where((builder) => {
      builder
        .where('month', 'LIKE', `%${month}%`)
    })
  }
  year(year: string) {
    this.$query.where((builder) => {
      builder
        .where('year', 'LIKE', `%${year}%`)
    })
  }

  

}
