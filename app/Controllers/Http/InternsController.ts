
import Intern from 'App/Models/Intern'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'


export default class InternsController {
  public async index({ auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    const interns = await Intern.all()
    return interns

  }

  public async create({ }: HttpContextContract) {
  }

  public async store({ request, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    const username = request.input('username')
    const email = request.input('email')
    const year = request.input('year')
    const stack = request.input('stacks')

    const intern = Intern.create({
      username: username,
      email: email,
      year: year,
    })
    await (await intern).related('stacks').create({
      stackname: stack
    })
    return intern
  }

  public async show({ params, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    const intern = await Intern.find(params.id)
    return intern
  }

  public async edit({ }: HttpContextContract) {
  }

  public async update({ request, params }: HttpContextContract) {

    const intern = await Intern.find(params.id)
    const username = request.input('username')
    const email = request.input('email')
    const year = request.input('year')
    if (intern) {
      intern.username = username;
      intern.email = email;
      intern.year = year;
      intern.save()
    }
    return intern

  }

  public async destroy({ params, response, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    const intern = await Intern.find(params.id)
    await intern?.delete()
    response.status(200).send({
      message: "Intern deleted Successfully"
    })

  }

  public async getYear({ params, auth }: HttpContextContract) {
    await auth.use('api').authenticate()
    const interns = await Intern
      .query()
      .where('year', '=', params.year)

    return interns;

  }

  public async getYearMonth({ params, auth, response }: HttpContextContract) {
    // await auth.use('api').authenticate()
      const interns = await Intern
        .query()
        .where('year', '=', params.year).andWhere('month', '=', params.month)

      response.json({
        interns
      });
  
  }

  public async internsFilter({ request }: HttpContextContract): Promise<Intern[]> {

    return Intern.filter(request.qs()).exec();
  }


  public async internsFilterr({ response }: HttpContextContract) {
  return await this.internsFilter;
    // var dataa = data.toString()
    // console.log(dataa)
    // response.send(dataa)
  }
}
