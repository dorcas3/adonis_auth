import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'

export default class UsersController {
  public async register({ request}: HttpContextContract) {

    const email = request.input('email')
    const password = request.input('password')

    const user = User.create({
      email:email,
      password:password
    });
    
    return user

  }

  public async login({ request,response,auth }: HttpContextContract) {
    const email = request.input('email')
    const password = request.input('password')
    try {
      const token = await auth.use('api').attempt(email, password)
      return token
    } catch {
      return response.badRequest('Invalid credentials')
    }
  }

  public async logout({ auth}: HttpContextContract) {
    await auth.use('api').revoke()
    return {
      revoked: true
    }
  }
}
