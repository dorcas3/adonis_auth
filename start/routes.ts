/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})
// interns controller
Route.resource("interns","InternsController").apiOnly()

// interns controller
// Route.post('register','UsersController.register')
Route.post('login','UsersController.login')
Route.post('logout','UsersController.logout')

Route.post("register", async (ctx) => {
  const { default: UsersController } = await import(
    "App/Controllers/Http/UsersController"
  );
  return new UsersController().register(ctx);
});
Route.get("interns/year/:year","InternsController.getYear")
Route.get("interns/year-month/:year/:month","InternsController.getYearMonth")
Route.get("interns-filter/","InternsController.internsFilterr")